# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase


class UomUnitTestCase(ModuleTestCase):
    """Test module"""
    module = 'product_uom_cat_unit'

    def setUp(self):
        super(UomUnitTestCase, self).setUp()


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UomUnitTestCase))
    return suite
